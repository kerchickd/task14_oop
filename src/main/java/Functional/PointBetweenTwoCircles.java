package Functional;

import Figure.Circle;
import Pair.Pair;
import Point.Point2D;

import java.util.ArrayList;
import java.util.List;

public class PointBetweenTwoCircles {

    private static boolean isBetween(Circle first, Circle second, Point2D point) {
        Point2D firstCenter = first.getCenter();
        Point2D secondCenter = second.getCenter();
        boolean checkFirst = Math.pow((point.getX() - firstCenter.getX()), 2) +
                Math.pow((point.getY() - firstCenter.getY()), 2) -
                Math.pow(first.getRadius(), 2) < 0 + 1E-8;
        boolean checkSecond = Math.pow((point.getX() - secondCenter.getX()), 2) +
                Math.pow((point.getY() - secondCenter.getY()), 2) -
                Math.pow(second.getRadius(), 2) < 0 + 1E-8;
        return checkFirst && checkSecond;
    }

    public static Pair<Pair<Circle, Circle>, List<Point2D>> getMostMatch(List<Circle> listOfCircles,
                                                                                             List<Point2D> listOfPoints) {
        List<Point2D> resultPoints = null;
        Pair<Circle, Circle> resultCircles = null;
        int maxAmount = Integer.MIN_VALUE;

        List<Circle> cloneListCircle = new ArrayList<>(listOfCircles);
        for (var firstCircle: listOfCircles) {
            cloneListCircle.remove(firstCircle);
            for (var secondCircle : cloneListCircle) {
                List<Point2D> pointsIntersection = new ArrayList<>();
                for (var point: listOfPoints) {
                    if (isBetween(firstCircle, secondCircle, point))
                        pointsIntersection.add(point);
                }
                if (pointsIntersection.size() > maxAmount) {
                    resultCircles = new Pair<>(firstCircle, secondCircle);
                    resultPoints = new ArrayList<>(pointsIntersection);
                    maxAmount = pointsIntersection.size();
                }
            }
        }
        return new Pair<>(resultCircles, resultPoints);
    }
}
