package FromDB;

import Point.Point2D;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PointsFromDB {

    private final String URL;
    private final String USERNAME;
    private final String PASSWORD;

    public PointsFromDB() {
        this.URL = "jdbc:mysql://localhost:3306/points";
        this.USERNAME = "user";
        this.PASSWORD = "root";
    }

    public PointsFromDB(String URL, String USERNAME, String PASSWORD) {
        this.URL = URL;
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
    }


    public List<Point2D> getPoints() {
        List<Point2D> result = new ArrayList<>();
        Connection connection;
        String query = "select * from points";
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            if (!connection.isClosed())
                System.out.println("Successful connection!");

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                double x = resultSet.getDouble(PointDBLabel.x.name());
                double y = resultSet.getDouble(PointDBLabel.y.name());
                result.add(new Point2D(x, y));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
