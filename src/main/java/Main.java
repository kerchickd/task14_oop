import Figure.Circle;
import FromDB.PointDBLabel;
import FromDB.PointsFromDB;
import Point.Point2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PointsFromDB data = new PointsFromDB();
        List<Point2D> listOfPoints = data.getPoints();
        List<Circle> circles = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            double x = sc.nextDouble();
            double y = sc.nextDouble();
            double r = sc.nextDouble();
            circles.add(new Circle(new Point2D(x, y), r));
        }
        Pair.Pair<Pair.Pair<Circle, Circle>, List<Point2D>> result = Functional.PointBetweenTwoCircles.getMostMatch(circles, listOfPoints);
        System.out.println(result.getLeft().getLeft() + " " + result.getLeft().getRight());
        System.out.println(result.getRight());
    }
}
/*
4 6 2
2 4 3
4 8 1
4 -1 4
9 6 2
 */
